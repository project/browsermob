<?php



/**
 * Implementation of hook_drush_help().
 */
function browsermob_drush_help($section) {
  switch ($section) {
    case 'meta:browsermob:title':
      return dt('Browsermob commands');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function browsermob_drush_command() {
  $items['browsermob-users'] = array(
    'description' => 'Print user information as javascript variables for using in Browsermob scripts.',
    'arguments' => array(
      'num_users' => 'Number of users to list',
    ),
    'options' => array(
      'password' => 'Reset the passwords of all users to this.',
    ),
    'aliases' => array('bmusers'),
  );
  return $items;
}

function drush_browsermob_users($num_users = INF) {
  $accounts = array();
  $count = 0;
  switch(drush_drupal_major_version()) {
    case 6:
      // TODO
      break;
    case 7:
    case 8:
      foreach (entity_load('user') as $user) {
        if ($user->uid > 2 && $count <= $num_users) {
          $accounts[] = $user->name;
          $count++;
        }
      }
      break;
  }

  if ($pass = drush_get_option('password', FALSE)) {
    $confirm = drush_confirm(dt('You are about to reset passwords to !pass. Do you want to continue?', array('!pass' => $pass)));
    // Update each user's password so they are all the same.
    drush_set_option('password', $pass);
    drush_set_option('backend', TRUE);
    foreach ($accounts as $name) {
      drush_invoke('user-password', array($name));
    }
  }
  else {
    // User can fill in later if reset is not used.
    $pass = '';
  }

  drush_print(browsermob_print_javascript($accounts, $pass));
}

function browsermob_print_javascript($accounts, $password) {
  $user_json = json_encode($accounts);
  $output = <<<EOF

    var drupalAccounts = $user_json;
    var drupalUser = drupalAccounts[Math.floor(Math.random() * drupalAccounts.length)];
    var drupalPass = '$password';
EOF;
  return $output;
}
